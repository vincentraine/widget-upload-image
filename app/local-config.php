<?php
// Database info
define('DB_NAME', 'customwidget');
define('DB_USER', 'root');
define('DB_PASSWORD', 'root');
define('DB_HOST', 'localhost'); // localhost:8888 if using MAMP defaults

// Show errors for local dev environment
ini_set( 'display_errors', E_ALL );
define( 'WP_DEBUG_DISPLAY', true );
define('WP_DEBUG', true);
